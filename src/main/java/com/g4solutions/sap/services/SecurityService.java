package com.g4solutions.sap.services;

public interface SecurityService {
    String findLoggedInUsername();
    void autologin(String username, String password);
}
