package com.g4solutions.sap.services;

import com.g4solutions.sap.dao.User;

public interface UserService {

    void save(User user);
    User findByUsername(String username);
}
