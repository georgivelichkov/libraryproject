package com.g4solutions.sap.controllers;

import com.g4solutions.sap.dao.Book;
import com.g4solutions.sap.repos.BookRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import java.util.Date;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    private BookRepository bookRepository;

    /////////////////////////////////////////////////////////////////////////////////////////////
    //////////////////////////////      BOOKS   ////////////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////
    @GetMapping({"","/"})
    public String index() {
        return "adminHome";
    }

    @GetMapping("/books")
    public ModelAndView books() {
        Iterable<Book> books = bookRepository.findAll();

        ModelAndView model = new ModelAndView();
        model.addObject("books", books);
        model.setViewName("adminListBooks");

        return model;
    }


    @GetMapping("/books/add")
    public String addBook() {
        return "adminAddBook";
    }

    @PostMapping("/books/add")
    public String createBook(@RequestParam("author") String author,
                             @RequestParam("releaseDate") String releaseDate,
                             @RequestParam("status") boolean status,
                             @RequestParam("title") String title) {

        Book book = new Book();
        book.setAuthor(author);
        book.setReleaseDate(new Date(releaseDate));
        book.setStatus(status);
        book.setTitle(title);

        bookRepository.save(book);

        return "redirect:/admin/books";
    }


    @GetMapping("/books/edit/{id}")
    public ModelAndView editBook(@PathVariable(value = "id") int id) {
        Book book = bookRepository.findById(id).get();
        ModelAndView model = new ModelAndView();
        model.addObject("book", book);
        model.setViewName("adminEditBook");

        return model;
    }

    @PostMapping("/books/edit/{id}")
    public String editBook(@PathVariable(value = "id") int id,
                           @RequestParam("author") String author,
                           @RequestParam("releaseDate") String releaseDate,
                           @RequestParam("status") boolean status,
                           @RequestParam("title") String title) {

        Book book = bookRepository.findById(id).get();
        book.setAuthor(author);
        book.setStatus(status);
        book.setTitle(title);
        book.setReleaseDate(new Date(releaseDate));

        bookRepository.save(book);

        return "redirect:/admin/books";
    }

    @GetMapping("/books/delete/{id}")
    public String deleteBook(@PathVariable(value = "id") int id) {
        bookRepository.deleteById(id);

        return "redirect:/admin/books";
    }
}
