package com.g4solutions.sap.controllers;

import com.g4solutions.sap.dao.Book;
import com.g4solutions.sap.dao.User;
import com.g4solutions.sap.repos.BookRepository;
import com.g4solutions.sap.repos.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private BookRepository bookRepo;

    @Autowired
    private UserRepository userRepo;

    @GetMapping({"","/"})
    public String index(Model model){
        model.addAttribute("books", bookRepo.findAll());
        return "userHome";
    }

    @GetMapping("/book/{id}")
    public String bookDetails(@PathVariable(value = "id") int id, Model model) {
        if(bookRepo.findById(id).isPresent()) {

            Book book = bookRepo.findById(id).get();
            model.addAttribute("book",book);
            return "bookDetail";

        }

        return "redirect:/404";
    }

    @GetMapping("/books")
    public String booksTaken( Model model ) {
        User user = userRepo.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());

        if(user != null) {

            model.addAttribute("books", user.getBooks());

            return "userBooks";
        }

        return "redirect:/404";
    }

    @GetMapping("/book/take/{id}")
    public String takeBook(@PathVariable(value = "id") int id) {
        if(bookRepo.findById(id).isPresent()) {
            Book book = bookRepo.findById(id).get();
            User user = userRepo.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());

            if(user != null) {

                Set<Book> userBooks = user.getBooks();
                userBooks.add(book);
                user.setBooks(userBooks);
                book.setStatus(false);
                bookRepo.save(book);

                userRepo.save(user);

            }

        }
        return "redirect:/user/books";
    }
}
