package com.g4solutions.sap.repos;

import com.g4solutions.sap.dao.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long>{
}

