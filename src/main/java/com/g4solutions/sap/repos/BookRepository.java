package com.g4solutions.sap.repos;

import com.g4solutions.sap.dao.Book;
import org.springframework.data.repository.CrudRepository;


public interface BookRepository extends CrudRepository<Book, Integer> {


}