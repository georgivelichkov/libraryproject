<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- breadcrumb area start -->
<section class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-area-content">
                    <h1>Всички книги</h1>
                </div>
            </div>
        </div>
    </div>
</section><!-- breadcrumb area end -->
<!-- portfolio section start -->
<section class="portfolio-area pt-60">
    <div class="container">
        <hr />
        <div class="row portfolio-item">
            <c:forEach items="${books}" var="book">
            <div class="col-lg-3 col-md-4 col-sm-6 soon released">
                <div class="single-portfolio">
                    <div class="single-portfolio-img">
                        <img src="<c:url value='/static/assets/img/portfolio/portfolio1.png'/>" alt="portfolio" />
                        <a href="/user/book/${book.id}" class="popup-youtube">
                            <i class="icofont icofont-read-book"></i>
                        </a>
                    </div>
                    <div class="portfolio-content">
                        <h2>${book.title}</h2>
                    </div>
                </div>
            </div>
            </c:forEach>
        </div>
    </div>
</section><!-- portfolio section end -->
