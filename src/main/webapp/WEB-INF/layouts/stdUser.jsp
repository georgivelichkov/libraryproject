<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>

<!DOCTYPE HTML>
<html lang="zxx">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>User Library</title>
    <!-- Favicon Icon -->
    <link rel="icon" type="image/png" href="<c:url value='/static/assets/img/favcion.png' /> " />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="<c:url value='/static/assets/css/bootstrap.min.css' />" media="all" />
    <!-- Slick nav CSS -->
    <link rel="stylesheet" type="text/css" href="<c:url value='/static/assets/css/slicknav.min.css' />" media="all" />
    <!-- Iconfont CSS -->
    <link rel="stylesheet" type="text/css" href="<c:url value='/static/assets/css/icofont.css '/> " media="all" />
    <!-- Owl carousel CSS -->
    <link rel="stylesheet" type="text/css" href="<c:url value='/static/assets/css/owl.carousel.css' />">
    <!-- Popup CSS -->
    <link rel="stylesheet" type="text/css" href="<c:url value='/static/assets/css/magnific-popup.css'/>">
    <!-- Main style CSS -->
    <link rel="stylesheet" type="text/css" href="<c:url value='/static/assets/css/style.css'/>" media="all" />
    <!-- Responsive CSS -->
    <link rel="stylesheet" type="text/css" href="<c:url value='/static/assets/css/responsive.css'/>" media="all" />
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<!-- Page loader -->
<div id="preloader"></div>
<!-- header section start -->
<header class="header">
    <div class="container">
        <div class="header-area">
            <div class="header-right">
                <ul>
                    <li><a href="#">Welcome Guest!</a></li>
                    <li><a class="login-popup" href="#">Login</a></li>
                </ul>
            </div>
            <div class="menu-area">
                <div class="responsive-menu"></div>
                <div class="mainmenu">
                    <ul id="primary-menu">
                        <li><a class="active" href="/user/">Всички книги</a></li>
                        <li><a href="/user/books">Взети книги</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</header>
<tiles:insertAttribute name="body"/>
<!-- footer section start -->
<footer class="footer">
    <div class="container">
    </div>
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 text-center text-lg-left">
                    <div class="copyright-content">
                        <p>&copy; 2018 LibraryProject. All Rights Reserved. Designed by octopas</p>
                    </div>
                </div>
                <div class="col-lg-6 text-center text-lg-right">
                    <div class="copyright-content">
                        <a href="#" class="scrollToTop">
                            Back to top<i class="icofont icofont-arrow-up"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer><!-- footer section end -->
<!-- jquery main JS -->
<script src="<c:url value='/static/assets/js/jquery.min.js'/>"></script>
<!-- Bootstrap JS -->
<script src="<c:url value='/static/assets/js/bootstrap.min.js'/>"></script>
<!-- Slick nav JS -->
<script src="<c:url value='/static/assets/js/jquery.slicknav.min.js'/>"></script>
<!-- owl carousel JS -->
<script src="<c:url value='/static/assets/js/owl.carousel.min.js'/>"></script>
<!-- Popup JS -->
<script src="<c:url value='/static/assets/js/jquery.magnific-popup.min.js'/>"></script>
<!-- Isotope JS -->
<script src="<c:url value='/static/assets/js/isotope.pkgd.min.js'/>"></script>
<!-- main JS -->
<script src="<c:url value='/static/assets/js/main.js'/>"></script>
</body>
</html>
