<%--
  Created by IntelliJ IDEA.
  User: gvbox
  Date: 2019-01-05
  Time: 19:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!-- breadcrumb area start -->
<section class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="breadcrumb-area-content">
                    <h1>${book.title}</h1>
                </div>
            </div>
        </div>
    </div>
</section><!-- breadcrumb area end -->
<!-- transformers area start -->
<section class="transformers-area">
    <div class="container">
        <div class="transformers-box">
            <div class="row flexbox-center">
                <div class="col-lg-5 text-lg-left text-center">
                    <div class="transformers-content">
                        <img src="<c:url value='/static/assets/img/slide2.png'/>" alt="about" />
                    </div>
                </div>
                <div class="col-lg-7">
                    <div class="transformers-content">
                        <h2>${book.title}</h2>
                        <p></p>
                        <ul>
                            <li>
                                <div class="transformers-left">
                                    Director:
                                </div>
                                <div class="transformers-right">
                                    ${book.author}
                                </div>
                            </li>
                            <li>
                                <div class="transformers-left">
                                    Release:
                                </div>
                                <div class="transformers-right">
                                    ${book.releaseDate}
                                </div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <a href="/user/book/take/${book.id}" class="theme-btn"><i class="icofont icofont-book-alt"></i> Take Book</a>
        </div>
    </div>
</section><!-- transformers area end -->
<!-- details area start -->
<section class="details-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-9">
                <div class="details-content">
                    <div class="details-overview">
                        <h2>Overview</h2>
                        <p>Humans are at war with the Transformers, and Optimus Prime is gone. The key to saving the future lies buried in the secrets of the past and the hidden history of Transformers on Earth. Now it's up to the unlikely alliance of inventor Cade Yeager, Bumblebee, a n English lord and an Oxford professor to save the world. Transformers: The Last Knight has a deeper mythos and bigger spectacle than its predecessors, yet still ends up being mostly hollow and cacophonous. The first "Transformers" movie that could actually be characterized as badass. Which isn't a bad thing. It may, in fact, be better.</p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section><!-- details area end -->
