<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">Добавяне на книга</h3>
    </div>
    <div class="panel-body">
        <form action="/admin/books/edit/${book.id}" class="form-control" method="POST">
            <label>Автор:</label>
            <input type="text" class="form-control" name="author" value="${book.author}"/>
            <label>Заглавие:</label>
            <input type="text" class="form-control" name="title" value="${book.title}"/>
            <label>Дата на издаване:</label>
            <input type="text" class="form-control" name="releaseDate" value=""/>
            <input type="submit" value="Submit"/>
        </form>
    </div>
</div>
