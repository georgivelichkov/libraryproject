<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">Добавяне на книга</h3>
    </div>
    <div class="panel-body">
        <form action="/admin/books/add" method="POST">
            <label>Автор:</label>
            <input type="text" class="form-control" name="author" />
            <label>Заглавие:</label>
            <input type="text" class="form-control" name="title" />
            <label>Дата на издаване:</label>
            <input type="text" class="form-control" name="releaseDate" />
            <label>Статус:</label>
            <div class="switch_box box_1">
                <input type="checkbox" class="switch_1" name="status">
            </div>
            <input type="submit" value="Submit" class="form-control"/>
        </form>
    </div>
</div>
