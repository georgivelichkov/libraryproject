<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="panel">
    <div class="panel-heading">
        <h3 class="panel-title">Bordered Table</h3>
    </div>
    <div class="panel-body">
        <table class="table table-bordered">
            <a href="/admin/books/add" class="btn btn-default btn-primary"><i class="fa fa-plus" aria-hidden="true"></i></a>
            <thead>
            <tr>
                <th>#</th>
                <th>Автор</th>
                <th>Заглавие</th>
                <th>Наличност</th>
                <th>Действия</th>
            </tr>
            </thead>
            <tbody>
            <c:forEach items="${books}" var="book">
                <td>${book.getId()}</td>
                <td>${book.getAuthor()}</td>
                <td>${book.getTitle()}</td>
                <td>${book.getStatus()}</td>
                <td>
                    <a href="/admin/books/edit/${book.getId()}" class="btn btn-default btn-primary"><i
                            class="fa fa-pencil" aria-hidden="true"></i></a>
                    <a href="/admin/books/delete/${book.getId()}" class="btn btn-default btn-primary"><i
                            class="fa fa-trash-o" aria-hidden="true"></i></a>
                </td>
            </c:forEach>
            </tbody>
        </table>
    </div>
</div>